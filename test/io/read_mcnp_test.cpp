#include <iostream>
#include "moab/Interface.hpp"
#include "TestUtil.hpp"
#include "Internals.hpp"
#include "moab/Core.hpp"
#include "MBTagConventions.hpp"
#include "moab/Types.hpp"
#include "moab/GeomTopoTool.hpp"

using namespace moab;

std::string mesh_test = TestDir + "/io/test_rect.meshtal";

int main()
{
  int result = 0;

  result += RUN_TEST();

  return result;
}

void read_file( Interface& moab, const char* input_file )
{
  ErrorCode rval = moab.load_file( input_file );
  CHECK_ERR(rval);
}

void test_check_dims()
{
  ErrorCode rval;
  Core core;
  Interface *mbi = &core;
  read_file(core, mesh_test.c_str());
  ScdInterface *scdiface;
  rval = mbi->query_interface(scdiface);
  CHECK_ERR(rval);
  std::vector<ScdBox*> boxes;
  rval = scdiface->find_boxes(boxes);
  CHECK_ERR(rval);
  ScdBox* box;
  box = boxes.front();

  // check that the box size is 3 in each dimension
  int I, J, K;
  box->box_size(I, J, K);
  CHECK_EQUAL(3, I);
  CHECK_EQUAL(3, J);
  CHECK_EQUAL(3, K);
}

// get number of entities
void test_check_entities()
{
}

// check entities are tagged
void test_check_tags()
{
  // check the tagged value
  // tagged values were created so they are the
  // product of I,J,K
  std::vector<double> tags;
  Tag newTag;
  mbi->tag_get_data(newTag, verts, &tags[0]);
  for (int i = 0; i < I; ++i){
    for (int j = 0; j < J; ++j){
      for (int k = 0; k < K; ++k){
	CHECK_EQUAL(I*J*K, );
      }
    }
  }
}


